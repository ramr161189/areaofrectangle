package com.tw.Exceptions;

public class NegativeDimensionException extends Exception{
    public NegativeDimensionException(String exceptionMessage) {
        super(exceptionMessage);
    }
}
