package com.tw.Exceptions;

public class ZeroDimensionException extends Exception{
    public ZeroDimensionException(String exceptionMessage) {
        super(exceptionMessage);
    }
}
