package com.tw.Rectangle;

import com.tw.Exceptions.NegativeDimensionException;
import com.tw.Exceptions.ZeroDimensionException;

public class Rectangle {
    double length;
    double breadth;
    public Rectangle(double length, double breadth) throws NegativeDimensionException, ZeroDimensionException {
        if(length<0 || breadth<0)
            throw new NegativeDimensionException("width and length can't be negative");
        else if(length==0 || breadth==0)
            throw new ZeroDimensionException("width and length can't be Zero");
        this.length=length;
        this.breadth=breadth;
    }

    public static Rectangle createSquare(double side) throws NegativeDimensionException, ZeroDimensionException {
        return new Rectangle(side,side);
    }

    double area() throws Exception{
        return this.length*this.breadth;
    }

    public double perimeter() {
        return 2*(this.length+this.breadth);

    }
}
