package com.tw.Rectangle;

import com.tw.Exceptions.NegativeDimensionException;
import com.tw.Exceptions.ZeroDimensionException;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RectangleTest {

    @Test
    public void shouldReturnAreaOfRectangleAsTwelveWhenBreadthIsFourAndLengthIsThree() throws Exception {

        Rectangle Rectangle = new Rectangle(2, 6);

        double result = Rectangle.area();

        assertThat(result, equalTo(12.0));


    }

    @Test
    public void shouldReturnPerimeterOfRectangleAsSixteenWhenBreadthIsSixAndLengthIsSix() throws Exception{


        Rectangle Rectangle = new Rectangle(2, 6);

        double result = Rectangle.perimeter();

        assertThat(result, equalTo(16.0));

    }

    @Test
    public void shouldReturnAreaOfSquareAsSixteenWhenSideIsFour() throws Exception {
        double side=4.0;

        Rectangle square = Rectangle.createSquare(side);

        double result = square.area();

        assertThat(result, equalTo(16.0));


    }

    @Test
    public void shouldReturnPerimeterOfSquareAsSixteenWhenSideIsFour() throws Exception{
        double side=4.0;

        Rectangle square = Rectangle.createSquare(side);

        double result = square.perimeter();

        assertThat(result, equalTo(16.0));

    }

    @Test
    void shouldReturnNegativeValueExceptionWhenBreadthIsTwoAndLengthIsMinusThree() {
        assertThrows(NegativeDimensionException.class,()-> new Rectangle(2.0,-3.0));
    }

    @Test
    void shouldReturnZeroValueExceptionWhenLengthIsZeroAndBreadthIsTwo() {
        assertThrows(ZeroDimensionException.class,()-> new Rectangle(0,2.0));

    }





}
